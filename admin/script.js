jQuery(document).ready(function($){
	up = $('#re-uploader');
	fix = $('#fix-uploader');
    $('input.re-upload').on("click",function(){
    	file = $(this).attr('data-file');
    	up.find('p#show_file').html("You are Uploading "+file);
    	up.find('input#filename').attr('value', file);
    	up.slideDown(200, 'linear');
    });
    up.find('input.nevermind').on("click",function(){
    	up.slideUp(200, 'linear');

    });

    $('input.fix-upload').on("click",function(){
    	file = $(this).attr('data-file');
    	fix.find('p#show_file').html("You are Uploading "+file);
    	fix.find('input#filename').attr('value', file);
    	fix.slideDown(200, 'linear');
    });
    fix.find('input.nevermind').on("click",function(){
    	fix.slideUp(200, 'linear');
    });

});