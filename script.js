jQuery(document).ready(function($){
	$( "#dateNeed" ).datepicker();
    $("#essay-uploader").on('submit', function(){
        var checkThis = true;
        $(this).find("input").each(function(){
            if(inputCheck($(this)) == false){
                checkThis = false;
            }
        });
        $(this).find("textarea").each(function(){
            if(inputCheck($(this)) == false){
                checkThis = false;
            }
        });
        return checkThis;
    });
    
});

function inputCheck($this){
    if(jQuery.trim($this.attr('value')) == ""){
        $this.addClass('error');
        $this.siblings('span.required').addClass('error');
        return false;
    }
    else{
        $this.removeClass('error');
        $this.siblings('span.required').removeClass('error');
        return true;
    }
}