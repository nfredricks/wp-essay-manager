<?php
if(isset($_POST['filename'])){
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
    global $wpdb;
    $file = $_POST['filename'];
    $current_user = wp_get_current_user();
    $editor = $current_user->user_login;
    $table_name = $wpdb->prefix . "essay_manager";

    if(isset($_POST['stop_edit'])){
        $editor = '';
    }
    if(isset($_POST['delete_essay'])){
        if($wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE filename = %s", $file)) != false){
            rename(dirname(__FILE__)."/uploaded/".$file, dirname(__FILE__)."/deleted/".$file);
        }
        else{
            echo "error removing file";
        }
    }
    else{
        $update = array('editor' => $editor);
        $where = array('filename' => $file );
        $wpdb->update($table_name, $update, $where);
    }
    wp_redirect( admin_url()."admin.php?page=uploaded-essay-manager" ); exit;  
}


?>