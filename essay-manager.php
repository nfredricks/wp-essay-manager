<?php
/*
Plugin Name: Essay Editing Manager
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: Allows users to uploade essays or any type of document to be edited by moderators or tutors
Version: 1.0
Author: Nicholas Fredricks
Author URI: http://logikgate.com
License: GPL2

Copyright 2012  Nicholas Fredricks  (email : nfredricks@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


//make all inputs required on upload form
//add delete functionality to register.php


add_filter('the_posts', 'conditionally_add_scripts_and_styles_essay'); // the_posts gets triggered before wp_head
function conditionally_add_scripts_and_styles_essay($posts){
    if (empty($posts)){ return $posts;}

    $shortcode_found = false; // use this flag to see if styles and scripts need to be enqueued
    foreach ($posts as $post) {
        if (stripos($post->post_content, '[your-essays]') !== false || stripos($post->post_content, '[essay-upload]') !== false ) {
            $shortcode_found = true; // bingo!
            break;
        }
    }
    if ($shortcode_found) {
        wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('essay-manager-style', plugins_url('style.css', __FILE__));
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('essay-manager-js', plugins_url('script.js', __FILE__), array('jquery','jquery-ui-datepicker'));
    }


 
    return $posts;
    

}




add_action( 'admin_menu', 'essay_manager' );

function essay_manager() {
    
    $page = add_menu_page('Essay Manager Options', 'Essay Manager', 'manage_options', 'uploaded-essay-manager', 'essay_settings');
    add_action('admin_print_styles-' . $page, 'essay_manager_admin_styles');

}

function essay_manager_admin_styles() {
    wp_enqueue_style('essay-manager-style', plugins_url('style.css', __FILE__));
    wp_enqueue_script('essay-manager-js', plugins_url('admin/script.js', __FILE__), 'jquery');
}


function essay_settings() {
 
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
 // variables for the field and option names 

    $hidden_field_name_essay = 'essay_submit_hidden';

   $exts_name = 'allowed_exts';


    // Read in existing option value from database
    $exts_val = get_option( $exts_name );

    // See if the user has posted us some information
    // If they did, this hidden field will be set to 'Y'

   if( isset($_POST[ $hidden_field_name_essay ]) && $_POST[ $hidden_field_name_essay ] == 'Y' ) {
        // Read their posted value
        $exts_val = $_POST[ $exts_name ];
        update_option( $exts_name, $exts_val );


        // Put an settings updated message on the screen

?>
<div class="updated"><p><strong><?php _e('settings saved.'); ?></strong></p></div>

<?php
}



    // Now display the settings editing screen

    echo '<div class="wrap">';
    echo "<h2>Essay Manager Options</h2>";
    
?>
<form name="essay-admin" id="essay-admin" method="post" action="">
<input type="hidden" name="<?php echo $hidden_field_name_essay; ?>" id="<?php echo $hidden_field_name_essay; ?>" value="Y">

<p><?php _e("Allowed Document Extensions"); ?> 
<input type="text" name="<?php echo $exts_name; ?>" value="<?php echo $exts_val; ?>">
</p>

<p class="submit">
<input type="submit" name="Submit" class="button-primary" value="Save Changes" >
</p>
</form>

<?php

    // header

    echo "<h2>Pending Essays</h2>";
    // settings form/
do_shortcode("[pending-essays]");
    


echo "<h2>Essays you are Editing</h2>";
do_shortcode("[admin-essays]");

echo "<h2>Essays you have Finished</h2>";
do_shortcode("[finished-essays]");


?>
</div>
<?php
 
}


function essay_install () {
   global $wpdb;
   $table_name = $wpdb->prefix . "essay_manager"; 

   $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      tup datetime DEFAULT '00-00-0000 00:00:00' NOT NULL,
      tre datetime DEFAULT '00-00-0000 00:00:00' NOT NULL,
      uploadee tinytext NOT NULL,
      editor tinytext NOT NULL,
      filename tinytext NOT NULL,
      re_filename tinytext NOT NULL,
      dateNeed datetime DEFAULT '00-00-0000' NOT NULL,
      comment text NOT NULL,
      UNIQUE KEY id (id)
    );";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}

function essay_install_data () {

    add_option('allowed_exts');
    update_option('allowed_exts', 'doc,docx,pdf,txt');

}


register_activation_hook(__FILE__,'essay_install');
register_activation_hook(__FILE__,'essay_install_data');

/*
    Needs detection of reuploaded document. Will have download option for reuploaded doc and original doc.
    checks if logged in. provides button to login/register pages.

    the uploader needs to do something to protect against over writting of duplicate name files

*/

function upload_essay_display(){
    $exts_val = get_option('allowed_exts');
    $exts = explode(',', $exts_val);

//needs to check to see if logged in
if(is_user_logged_in()){
    if($_GET['err'] == "file"){
        echo "<p class=error><strong>ERROR:</strong> Incorrect File Type</p>";
    }
ob_start();
?>

    <form action="<?php echo plugins_url('upload.php', __FILE__); ?>" method="POST" enctype="multipart/form-data" id="essay-uploader">
        <p><input name="userfile" type="file" size="50"><br>
        <strong>Supported File Types:</strong> <?php for($i=0; $i<(count($exts)-1); $i++) {echo trim($exts[$i]).", ";} echo trim($exts[count($exts)-1]); ?></p>
        <p>When do you need this file edited by? <span class="required"> (required)</span><br>
        <input type="text" value="" name="dateNeed" id="dateNeed"></p>
        <p>Tell us a little about your essay:<span class="required"> (required)</span><br>
        <textarea name=comment rows=7 cols=100></textarea>
        </p>
        <p><input type="submit" name="submit" value="Upload" ></p>
    </form>
<?
return ob_get_clean();
}
else{
    display_essay_login();
}


}

/*
    Needs detection of reuploaded document. Will have download option for reuploaded doc and original doc.
    checks if logged in. provides button to login/register pages.
*/

function your_essay_display(){
if(is_user_logged_in()){
    global $wpdb;
    $table_name = $wpdb->prefix . "essay_manager"; 
    $current_user = wp_get_current_user();
    $user_name = $current_user->user_login;
    $table_name = $wpdb->prefix . "essay_manager"; 

    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE uploadee = %s", $user_name));
    if($result != NULL){
	ob_start();
    ?>
    <table class='your_essays'>
        <thead>
            <tr>
                <th>File</th>
                <th>Uploaded On</th>
                <th>Date Needed By</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
    <?php
    foreach ($result as $key => $essay) {
        list($year, $month, $day) = explode("-", $essay->dateNeed);
        $dateNeed = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        list($date_up, $time_up) = explode(" ", $essay->tup);
        list($year, $month, $day) = explode("-", $date_up);
        $date_up = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        $time_up = date("g:i a", strtotime($time_up));
        $datetime_up = $date_up." ".$time_up;

        ?>
        <tr>
            <td><a href='<?php echo plugins_url("uploaded/".$essay->filename, __FILE__);?>'><?php echo $essay->filename; ?></a></td>
            <td><?php echo $datetime_up; ?></td>
            <td><?php echo $dateNeed; ?></td>
        <?php
        //we now must do some checks to figure out the status
        $status = "Editing has not begun";
        if($essay->editor != ""){
            if($essay->tre != "0000-00-00 00:00:00"){
                $status = "<a href='".plugins_url("completed/".$essay->re_filename, __FILE__)."'>Editing has finished!</a>";
            }   
            else{
                $status = "Being edited by ".$essay->editor;
            }
        }
        echo "<td>".$status."</td>";
        echo "</tr>";

    }

    ?>
        </tbody>
    </table>
<?php
return ob_get_clean();
}
else{
?>
<h3>You have not uploaded any essays for editing :(</h3>
    <?php
}
}
else{
    display_essay_login();
}
}
/*
    Needs option to reupload the document. This will notify the student of the reupload and change the status of it in their Your_essay_display. It will also put this file in the reuploaded directory.
*/
function admin_essay_display(){
    global $wpdb;
    $current_user = wp_get_current_user();
    $admin_name = $current_user->user_login;
    $table_name = $wpdb->prefix . "essay_manager"; 

    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE editor = %s AND re_filename = '' ", $admin_name));
    if($result != NULL){
    ?>
      <table class='pending_essays'>
        <thead>
            <tr>
                <th>Uploader</th>
                <th>Uploaded On</th>
                <th>Date Needed By</th>
                <th>Uploader Comments</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
    <?php
    foreach ($result as $key => $essay) {
        list($year, $month, $day) = explode("-", $essay->dateNeed);
        $dateNeed = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        list($date_up, $time_up) = explode(" ", $essay->tup);
        list($year, $month, $day) = explode("-", $date_up);
        $date_up = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        $time_up = date("g:i a", strtotime($time_up));
        $datetime_up = $date_up." ".$time_up;

        ?>
        <tr>
            <td><?php echo $essay->uploadee; ?></td>
            <td><?php echo $datetime_up; ?></td>
            <td><?php echo $dateNeed; ?></td>
            <td><?php echo $essay->comment; ?></td>
            <td>
                <form action="<?php echo plugins_url("uploaded/".$essay->filename, __FILE__);?>" method=post class="inline_form">
                    <input type=submit name=submit value=Download>
                </form>
                <input type=button name="re-upload" class="re-upload" value="Re-Upload" data-file="<?php echo $essay->filename; ?>">
                <form action="<?php echo plugins_url("register.php", __FILE__);?>" method=post class="inline_form">
                    <input type=hidden name=filename value="<?php echo $essay->filename; ?>">
                    <input type=hidden name='stop_edit' value="true">
                    <input type=submit name=submit value="Unchoose">
                </form>
            </td>
        </tr>
<?php
    }
    $exts_val = get_option('allowed_exts');
    $exts = explode(',', $exts_val);
    ?>
        </tbody>
    </table>
    <div id="re-uploader">
        <p id="show_file"></p>
        <form action="<?php echo plugins_url('upload.php', __FILE__); ?>" method="POST" enctype="multipart/form-data">
            <p><input type=hidden id="filename" name="filename" value=""><input name="userfile" type="file" size="50"><br>
            <strong>Supported File Types:</strong> <?php for($i=0; $i<(count($exts)-1); $i++) {echo trim($exts[$i]).", ";} echo trim($exts[count($exts)-1]); ?></p>
            <p><input type=submit name=submit value=Upload><input type=button value=Cancel class=nevermind></p>
        </form>
        
    </div>


<?php
}
else{
    echo "<h3>You are not currently editing any documents</h3>";
}
}

/*
    Needs option to download pending essays and also a seperate option to choose to edit it. Nothing happens when downloaded, only when someone chooses to edit it, it then moves to their currently editing seciton and is hidden from pending
    This should also have a full delete option incase of inappropriate upload by student.
*/
function pending_essay_display(){

    global $wpdb;
    $table_name = $wpdb->prefix . "essay_manager"; 

    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE editor = ''"));

    if($result != NULL){
    ?>
      <table class='pending_essays'>
        <thead>
            <tr>
                <th>Uploader</th>
                <th>Uploaded On</th>
                <th>Date Needed By</th>
                <th>Uploader Comments</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
    <?php
    foreach ($result as $key => $essay) {
        list($year, $month, $day) = explode("-", $essay->dateNeed);
        $dateNeed = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        list($date_up, $time_up) = explode(" ", $essay->tup);
        list($year, $month, $day) = explode("-", $date_up);
        $date_up = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        $time_up = date("g:i a", strtotime($time_up));
        $datetime_up = $date_up." ".$time_up;

        ?>
        <tr>
            <td><?php echo $essay->uploadee; ?></td>
            <td><?php echo $datetime_up; ?></td>
            <td><?php echo $dateNeed; ?></td>
            <td><?php echo $essay->comment; ?></td>
            <td>
                <form action="<?php echo plugins_url("uploaded/".$essay->filename, __FILE__);?>" method=post class="inline_form">
                    <input type=submit name=submit value=Download>
                </form>
                <form action="<?php echo plugins_url("register.php",__FILE__); ?>" method=post class="inline_form">
                    <input type=hidden name=filename value="<?php echo $essay->filename; ?>">
                    <input type=submit name=submit value=Choose>
                </form>
                <form action="<?php echo plugins_url("register.php",__FILE__); ?>" method=post class="inline_form">
                    <input type=hidden name="delete_essay" value="true">
                    <input type=hidden name=filename value="<?php echo $essay->filename; ?>">
                    <input type=submit name=submit value=Delete>
                </form>
            </td>
        </tr>
<?php
    }
    ?>
        </tbody>
    </table>

<?php
}
else{
    echo "<h3>There are no pending documents</h3>";
}
}

function finished_essay_display(){
    global $wpdb;
    $current_user = wp_get_current_user();
    $admin_name = $current_user->user_login;
    $table_name = $wpdb->prefix . "essay_manager"; 

    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE editor = %s AND re_filename != '' ", $admin_name));
    if($result != NULL){
    ?>
      <table class='pending_essays'>
        <thead>
            <tr>
                <th>Uploader</th>
                <th>Uploaded On</th>
                <th>Date Needed By</th>
                <th>Uploader Comments</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
    <?php
    foreach ($result as $key => $essay) {
        list($year, $month, $day) = explode("-", $essay->dateNeed);
        $dateNeed = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        list($date_up, $time_up) = explode(" ", $essay->tup);
        list($year, $month, $day) = explode("-", $date_up);
        $date_up = date("m-d-Y", mktime('0','0','0',$month,$day,$year));
        $time_up = date("g:i a", strtotime($time_up));
        $datetime_up = $date_up." ".$time_up;

        ?>
        <tr>
            <td><?php echo $essay->uploadee; ?></td>
            <td><?php echo $datetime_up; ?></td>
            <td><?php echo $dateNeed; ?></td>
            <td><?php echo $essay->comment; ?></td>
            <td>
                <form action="<?php echo plugins_url("uploaded/".$essay->filename, __FILE__);?>" method=post class="inline_form">
                    <input type=submit name=submit value=Original>
                </form>
                <form action="<?php echo plugins_url("completed/".$essay->re_filename, __FILE__);?>" method=post class="inline_form">
                    <input type=submit name=submit value=Edited>
                </form>
                <input type=button name="re-upload" class="fix-upload" value="Re-Upload" data-file="<?php echo $essay->filename; ?>">
            </td>
        </tr>
<?php
    }
    $exts_val = get_option('allowed_exts');
    $exts = explode(',', $exts_val);
    ?>
        </tbody>
    </table>
    <div id="fix-uploader">
        <p id="show_file"></p>
        <form action="<?php echo plugins_url('upload.php', __FILE__); ?>" method="POST" enctype="multipart/form-data">
            <p><input type=hidden id="filename" name="filename" value=""><input name="userfile" type="file" size="50"><br>
            <strong>Supported File Types:</strong> <?php for($i=0; $i<(count($exts)-1); $i++) {echo trim($exts[$i]).", ";} echo trim($exts[count($exts)-1]); ?></p>
            <p><input type=submit name=submit value=Upload><input type=button value=Cancel class=nevermind></p>
        </form>
        
    </div>


<?php
}
else{
    echo "<h3>Looks like you have not finished editing any documents. Pick one from above to get started :]</h3>";
}
}


function display_essay_login(){
    ?>
        <h2>It doesn't look like you are logged in :(</h2>
        <p>You must be logged in to use this functionality.</p>
        <div class='login-block'>
            <p><a href='<?php echo home_url()."/wp-login.php"; ?>'>Login or Register Here</a></p>
        </div>
    <?php
}



add_shortcode( 'essay-upload', 'upload_essay_display' );
add_shortcode( 'your-essays', 'your_essay_display' );
add_shortcode( 'admin-essays', 'admin_essay_display' );
add_shortcode( 'finished-essays', 'finished_essay_display' );
add_shortcode( 'pending-essays', 'pending_essay_display' );


?>
