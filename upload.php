<?php

require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );


$filename = mysql_escape_string($_FILES["userfile"]["name"]);
$size = mysql_escape_string(strip_tags($_FILES["userfile"]["size"]/1024));
$tempname = mysql_escape_string(strip_tags($_FILES["userfile"]["tmp_name"]));

if(isset($_POST['filename'])){
    $target = dirname(__FILE__)."/completed/".$filename;
    $old_name = $_POST['filename'];
    $re = true;
}

else{
    $old_name = '';
    $dateNeed = strip_tags($_POST["dateNeed"]);
    list($month, $day, $year) = explode("/", $dateNeed);
    $dateNeed = date("Y-m-d", mktime('0','0','0',$month,$day,$year));
    $comment = strip_tags($_POST["comment"]);
    $target = dirname(__FILE__)."/uploaded/".$filename;
    $re = false;
}

//check to make sure file type is OK
$ext = pathinfo($filename, PATHINFO_EXTENSION);
$exts_val = get_option( 'allowed_exts' );
$exts = explode(',', $exts_val);
$correct_filetype = false;
for($i=0; $i<count($exts); $i++){
    if($exts[$i] == $ext){
        $correct_filetype = true;
    }
}



if($correct_filetype){
    upload_file($tempname, $target, $filename, $dateNeed, $comment, $re, $old_name, $ext);
}
else{
    wp_redirect( home_url()."/essay-editing?err=file" ); exit;
}


function upload_file($source, $target,$filename, $dateNeed, $comment, $re, $old_name, $ext) {
global $wpdb;
$current_user = wp_get_current_user();
$username = $current_user->user_login;
$table_name = $wpdb->prefix . "essay_manager";
$i = false;
$j=1;
while ($i == false) {
    if(file_exists($target)){
        $dir = dirname($target);
        $file = basename($target, ".".$ext);
        $target = $dir."/".$file.$j.".".$ext;
        $j++;
    }
    else{
        $i = true;
        $filename = basename($target);
    }
}



if(move_uploaded_file($source, $target)) {

    if($re){
        //update db
        $update = array('tre' => current_time('mysql'), 're_filename' => $filename);
        $where = array('filename' => $old_name );
        $wpdb->update($table_name, $update, $where);
		//should email student here
        wp_redirect( admin_url()."admin.php?page=uploaded-essay-manager" ); exit;     
    }
    else{
        //make db entry
        $wpdb->insert( $table_name, array( 'tup' => current_time('mysql'), 'tre' => '', 'uploadee' => $username, 'editor' => '', 'filename' => $filename,'re_filename' => '', 'dateNeed' => $dateNeed, 'comment' => $comment) );  
        wp_redirect( home_url()."/your-essays" ); exit;
    }
}

else{
    echo "There was an error uploading the file, please try again!";
}  



}



?>
